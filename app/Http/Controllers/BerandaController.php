<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artikel;
use App\Models\Mood;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class BerandaController extends Controller
{
    public function index()
    {
        $id_user = Auth::id();
        $data = Mood::whereMonth('created_at', now()->month)->where("id_user", $id_user)->get();

        $happyCount = 0;
        $sadCount = 0;
        $netralCount = 0;

        foreach ($data as $entry) {
            switch ($entry->mood) {
                case 'Happy':
                    $happyCount++;
                    break;
                case 'Sad':
                    $sadCount++;
                    break;
                case 'Netral':
                    $netralCount++;
                    break;
            }
        }

        $happyPercentage = 0;
        $sadPercentage = 0;
        $netralPercentage = 0;

        if ($data) {


        $totalMood = count($data);
            if ($totalMood != 0) {
                $happyPercentage = ($happyCount / $totalMood) * 100;
                $sadPercentage = ($sadCount / $totalMood) * 100;
                $netralPercentage = ($netralCount / $totalMood) * 100;
            }
        }


        return view("user.beranda", [
             "artikel" => Artikel::orderBy("id", "DESC")->limit(6)->get(),
             "happy" => round($happyPercentage),
             "sad" => round($sadPercentage),
             "netral" => round($netralPercentage),
        ]);
    }

    public function mood(Request $request)
    {

        $id_user = Auth::id();

        return view("user.mood", [  
             "mood" => Mood::whereMonth('created_at', now()->month)->where("id_user", $id_user)->orderby("created_at", "DESC")->get(),
             "user" => User::find($id_user),
        ]);
    }

    public function artikel()
    {

        return view("user.artikel", [
             "artikel" => Artikel::orderBy("id", "DESC")->get(),
        ]);
    }

    public function artikel_detail(Artikel $artikel)
    {

        return view("user.artikel-detail", [
             "artikel" => $artikel,
        ]);
    }

    public function update_profile (Request $request )
    {
        $id_user = Auth::id();
        $user = User::find($id_user);
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->location = $request->location;
        $user->save();

        return back();
    }
}
