<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    //
    public function login()
    {
        return view("user.signin");
    }

    public function auth(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route("beranda");
        }

        return redirect()->route("signin")->with('error', 'Username atau Password Salah!');
    }

    public function signup()
    {
        return view("user.signup");
    }

    public function store(Request $request)
    {
        $request->validate([
            "name" => "required",
            "username" => "required",
            "email" => "required",
            "password" => "required",
            "phone" => "required",
        ]);

        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = ($request->password);
        $user->phone = $request->phone;
        $user->role = "user";
        $user->img = "";
        $user->save();

        return back()->with("success", "Registrasi Berhasil, Silahkan Login!");
    }

    public function logout(Request $request)
    {
        Auth::logout();
        return redirect()->route("signin");
    }
}
