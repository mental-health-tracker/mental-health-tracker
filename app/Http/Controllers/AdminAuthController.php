<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminAuthController extends Controller
{
    //

    public function index()
    {
        return view("dashboard.signin");
    }

    public function auth(Request $request)
    {
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
            return redirect()->route("dashboard.index");
        }

        return redirect()->route("dashboard.signin")->with('error', 'Username atau Password Salah!');
    }

    public function logout()
    {
        Auth::logout();
        return redirect()->route("dashboard.signin");
    }
}
