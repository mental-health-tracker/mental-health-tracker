<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mood;
use Illuminate\Support\Facades\Auth;


class MoodController extends Controller
{
    //
    public function index()
    {
        return view("dashboard.mood.index", [
            "hal" => "Mood",
            "data" => Mood::orderby("created_at", "DESC")->get(),
        ]);
    }

    public function store(Request $request, $moodUser)
    {
        $mood = new Mood;
        $mood->id_user = Auth::id();
        $mood->mood = $moodUser;
        $mood->save();

        return redirect()->route("mood")->with('success', 'Mood Anda Berhasil Diperbaharui!');
    }

    public function destroy(Mood $mood)
    {
        $mood->delete();
        return redirect()->route("dashboard.mood.index")->with('success', 'Mood Berhasil Dihapus!');
    }
}
