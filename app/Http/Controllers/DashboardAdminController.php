<?php

namespace App\Http\Controllers;

use App\Models\Artikel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardAdminController extends Controller
{
    public function index()
    {
        $this->cekLogin();

        return view(
            "dashboard.index",
            [
                "artikel" => Artikel::count(),
            ]
        );
    }

    public function cekLogin()
    {
        $user = Auth::user();

        if (!$user || $user->role != "admin") {
            echo "<script>";
            echo "
                location.href = ' " . url("/admin/signin") . "';
                ";
            echo "</script>";
            die;
        }
    }
}
