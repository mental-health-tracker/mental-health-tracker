<?php

namespace App\Http\Controllers;

use App\Models\Tour;
use Illuminate\Http\Request;

class TourController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view("admin.tour.index", [
            "hal" => "tour",
            "data" => Tour::get(),
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view("admin.tour.create", [
            "hal" => "tour",
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            "image" => "required",
            "desk" => "required",
        ]);

        $filename  = "";

        if ($request->file("image")) {
            $file = $request->file("image");
            $filename = time() . "."  . $file->getClientOriginalExtension();
            $file->move("assets/img/tour", $filename);
        }

        $tour = new Tour;
        $tour->image = $filename;
        $tour->desk = $request->desk;
        $tour->save();

        return redirect()->route("admin.tour.index")->with("success", "Data Tour Berhasil Ditambah!");
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Tour $tour)
    {
        return view("admin.tour.edit", [
            "hal" => "tour",
            "tour" => $tour,
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Tour $tour)
    {
        $request->validate([
            "desk" => "required",
        ]);

        if ($request->file("image")) {
            $filename  = "";
            $file = $request->file("image");
            $filename = time() . "."  . $file->getClientOriginalExtension();
            $file->move("assets/img/tour", $filename);
            $tour->image = $filename;
        }

        $tour->desk = $request->desk;
        $tour->save();

        return redirect()->route("admin.tour.index")->with("success", "Data Tour Berhasil Diupdate!");
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Tour $tour)
    {
        $tour->delete();
        return redirect()->route("admin.tour.index")->with("success", "Data Tour Berhasil Dihapus!");
    }
}
