<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Todolist;


class TodolistController extends Controller
{
    //
    public function index()
    {
        $id_user = Auth::id();
        return view("user.todolist.index", [
            "hal" => "Todolist",
            "data" => Todolist::where("id_user", $id_user)->get(),
        ]);
    }

    public function store(Request $request)
    {
        $request->validate(["todo" => "required"]);
        $id_user = Auth::id();

        $todo = new Todolist;
        $todo->todo = $request->todo;
        $todo->id_user = $id_user;
        $todo->save();

        return redirect()->route("todolist.index")->with('success', 'Todolist Berhasil Ditambah!');
    }

    public function destroy($id)
    {
        $todo = Todolist::find($id);
        $todo->delete();
        return redirect()->route("todolist.index")->with('success', 'Todolist Berhasil Dihapus!');
    }
}
