<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\User;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $user = User::where("role", "user")->get();
        return response()->json($user, Response::HTTP_OK);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = ($request->password);
        $user->phone = $request->phone;
        $user->role = "user";
        $user->img = "";
        $user->save();

        $response = [
            'Success' => 'New Category Created',
        ];
        return response()->json($response, Response::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $user = User::find($id);
        return response()->json($user, Response::HTTP_OK);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
        $user = User::find($id);
     $user->name = $request->name;
        $user->username = $request->username;
        $user->email = $request->email;
        $user->password = ($request->password);
        $user->phone = $request->phone;
        $user->role = "user";
        $user->img = "";
        $user->save();

        $response = [
            'Success' => 'New Category Updated',
        ];
        return response()->json($response, Response::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $user = User::find($id);
        $user->delete();
        return response()->json(['success' => 'Category deleted successfully.']);
    }
}
