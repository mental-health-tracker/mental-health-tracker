<?php

namespace App\Http\Controllers;

use App\Models\Book;
use App\Models\Cerita;
use App\Models\KategoriMusic;
use App\Models\Music;
use App\Models\Tour;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardUserController extends Controller
{

    public function __construct()
    {
        // dd($user);
    }

    public function index()
    {
        return view("user.index", [
            "page" => "Home",
            "cerita" => Cerita::orderby("id", "DESC")->get(),
        ]);
    }

    public function konsultasi()
    {
        return view("user.konsultasi", [
            "page" => "Konsultasi",
        ]);
    }

    public function rekomendasi()
    {
        return view("user.rekomendasi", [
            "page" => "Rekomendasi",
            "kategori" => KategoriMusic::orderby("id", "DESC")->get(),
            "music" => Music::orderby("id", "DESC")->get(),
            "book" => Book::orderby("id", "DESC")->get(),
            "tour" => Tour::orderby("id", "DESC")->get(),
        ]);
    }

    public function about()
    {
        return view("user.about", [
            "page" => "About",
        ]);
    }

    public function informasi()
    {
        return view("user.informasi", [
            "page" => "Informasi",
        ]);
    }
}
