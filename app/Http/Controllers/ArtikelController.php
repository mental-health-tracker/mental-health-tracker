<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artikel;

class ArtikelController extends Controller
{
    //
    public function index()
    {
        return view("dashboard.artikel.index", [
            "hal" => "Artikel",
            "data" => Artikel::get(),
        ]);
    }

    public function create($value='')
    {
        return view("dashboard.artikel.create", [
            "hal" => "Artikel",
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            "judul" => "required",
            "desk" => "required",
            "image" => "required",
        ]);

        $artikel = new Artikel;
        $artikel->judul = $request->judul;
        $artikel->desk = $request->desk;

        $filename  = "";

        if ($request->file("image")) {
            $file = $request->file("image");
            $filename = time() . "."  . $file->getClientOriginalExtension();
            $file->move("assets/img/artikel", $filename);
        }

        $artikel->img = $filename;
        $artikel->save();

       return redirect()->route("dashboard.artikel.index")->with("success", "Artikel Berhasil Ditambah!");
    }


    public function edit(Request $request, Artikel $artikel)
    {
        return view("dashboard.artikel.edit", [
            "hal" => "Artikel",
            "data" => $artikel,
        ]);
    }

    public function update(Request $request, Artikel $artikel)
    {
         $request->validate([
            "judul" => "required",
            "desk" => "required",
        ]);

        $artikel->judul = $request->judul;
        $artikel->desk = $request->desk;

        if ($request->file("image")) {
            $file = $request->file("image");
            $filename = time() . "."  . $file->getClientOriginalExtension();
            $file->move("assets/img/artikel", $filename);
            $artikel->img = $filename;
        }

        $artikel->save();

       return redirect()->route("dashboard.artikel.index")->with("success", "Artikel Berhasil Update!");
    }

    public function destroy(Artikel $artikel)
    {
        $artikel->delete();
        return redirect()->route("dashboard.artikel.index")->with("success", "Artikel Berhasil Dihapus!");
    }


}

