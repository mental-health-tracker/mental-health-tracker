<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <link rel="stylesheet" href="assets/css/beranda.css" />

  <title>Halaman Utama</title>
</head>

<body>

  <!-- Navbar -->
  <div class="navbar">
    <ul>
      <li><a href="{{ route('signup') }}">Sign Up</a></li>
      <li><a href="{{ route('signin') }}">Sign In</a></li>
    </ul>
  </div>
  <!-- End Navbar -->

  <!-- Hero -->
  <div class="hero">
    <img src="assets/img/logo-hero.png" alt="" />
  </div>
  <!-- End Hero -->

  <!-- Qoute -->
  <div class="qoute">
    <h4>Hai How’s your day?</h4>
    <p>
      “hidup ini singkat<br />mari kita nikmati setiap perjalanan sampai hari
      terakhir kita dibumi”
    </p>
  </div>
  <!-- End Qoute -->

  <!-- What -->
  <div class="what">
    <h4>Lihat apa yang kamu butuhkan saat ini</h4>

    <div class="box left-box">
      <img src="assets/img/img-1.png" alt="">
      <p>
        Dengan curhat dan mengutarakan isi hati dapat mengurangi
        riuhnya suara yang ada dikepalamu. Yuk Curhat Sekarang!
      </p>
    </div>

    <div class="box right-box">
      <p>
        Cari rekomendasi self healingmu dan me time untuk
        merilekskan semua beban dipundakmu.Cari suasana baru
        untuk memulai hari hari semangat baru
      </p>
      <img src="assets/img/img-2.png" alt="">
    </div>

    <div class="box left-box">
      <img src="assets/img/img-3.png" alt="">
      <p>
        Lakukan konsultasi apabila kamu tidak bisa
        menemukan solusi atas apa yang kamu alami dan jika kamu
        merasakan ada hal yang lain pada dirimu.
      </p>
    </div>

    <div class="box right-box">
      <p>
        Cari tahu lebih banyak mengenai apa itu mental health
      </p>
      <img src="assets/img/mg-4.png" alt="">
    </div>
  </div>
  <!-- End What -->

  <!-- Sekilas -->
  <div class="sekilas">
    <h4>sekilas tentang cerita mereka</h4>

    <div class="d-flex">

       @php
          $no = 1;
      @endphp

      @foreach ($cerita as $item)
          

      <div class="box">
        <h5 class="user c{{ $no }}">
          <img src="assets/img/profile-{{ $no }}.png" alt="">
          Anonymous
        </h5>
        <p>
          {{ $item->isi }}
        </p>
      </div>

        @php
          
          if ($no == 3) {
            $no = 1;
          }      

          $no++;
        @endphp

      @endforeach

    </div>
  </div>
  <!-- End Sekilas -->


  <!-- Footer -->
  <footer>
    <div class="info">

      <div class="box">
        <h5>Our Service</h5>
        <a href="">Curhat</a>
        <a href="">Konseling</a>
        <a href="">Me Time</a>
        <a href="">Self Care</a>
      </div>

      <div class="box">
        <h5>Recomendation</h5>
        <a href="">Music</a>
        <a href="">Book</a>
        <a href="">Tour</a>
        <a href="">Motivation</a>
      </div>

      <div class="box img">
        <img src="assets/img/logo.png" alt="">
      </div>

      <div class="box">
        <h5>Get to know us</h5>
        <a href="">Contact</a>
        <a href="">About Us</a>
        <a href="">Read Our Blogs</a>
      </div>

      <div class="box">
        <h5>Information</h5>
        <a href="">Mental Health</a>
      </div>


    </div>

    <div class="sosmed">
      <div class="col">
        <img src="assets/img/ig.png" alt="">
        <p>
          @ventavibes
        </p>
      </div>

      <div class="col">
        <img src="assets/img/em.png" alt="">
        <p>
          ventavibes@gmail.com
        </p>
      </div>

      <div class="col">
        <img src="assets/img/go.png" alt="">
        <p>
          http://www.ventavibes.com
        </p>
      </div>
    </div>
  </footer>
  <!-- End Footer -->

</body>

</html>