<nav class="sidebar sidebar-offcanvas" id="sidebar">
  <ul class="nav">
    <li class="nav-item">
      <div class="d-flex sidebar-profile">
        <div class="sidebar-profile-image">
          <img src="{{ asset("assets/dashboard/images/faces/face2.jpg") }}" alt="image">
          <span class="sidebar-status-indicator"></span>
        </div>
        <div class="sidebar-profile-name">
          <p class="sidebar-name">
            Administrator
          </p>
          <p class="sidebar-designation">
            Welcome
          </p>
        </div>
      </div>
     
      <p class="sidebar-menu-title">Dash menu</p>
    </li>
    <li class="nav-item">
      <a class="nav-link" href="{{ route("dashboard.index") }}">
        <i class="typcn typcn-device-desktop menu-icon"></i>
        <span class="menu-title">Dashboard</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route("dashboard.artikel.index") }}">
        <i class="typcn typcn-document-text menu-icon"></i>
        <span class="menu-title">Artikel</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route("dashboard.mood.index") }}">
        <i class="typcn typcn-compass menu-icon"></i>
        <span class="menu-title">History Mood User</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route("dashboard.user.index") }}">
        <i class="typcn typcn-user-add-outline menu-icon"></i>
        <span class="menu-title">Account User</span>
      </a>
    </li>

    <li class="nav-item">
      <a class="nav-link" href="{{ route("dashboard.logout") }}">
        <i class="typcn typcn-user-add-outline menu-icon"></i>
        <span class="menu-title">Logout</span>
      </a>
    </li>

  </ul>
</nav>

