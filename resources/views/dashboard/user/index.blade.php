@extends('dashboard.layout')

@section('content')

<div class="content-wrapper">
  <div class="row">

    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title judul-form">Tambah User</h4>

          <div class="form-floating mb-3">
                <label for="fullname">Full Name</label>
                <input
                  class="form-control border-black"
                  type="text"
                  id="fullname"
                  placeholder="Masukkan nama"
                  value=""
                  name="name"
                />
              </div>
              <div class="form-floating mb-3">
                <label for="floatingInput">Email address</label>
                <input
                  type="email"
                  class="form-control border-black"
                  id="floatingInput"
                  placeholder="name@example.com"
                  value=""
                  name="email"
                />
              </div>
              <div class="form-floating mb-3">
                <label for="floatingPhone">Phone number</label>
                <input
                  type="number"
                  class="form-control border-black"
                  id="floatingPhone"
                  placeholder="0341341"
                  value=""
                  name="phone"
                />
              </div>
              <div class="form-floating mb-3">
                <label for="floatingUsername">Username</label>
                <input
                  type="text"
                  class="form-control border-black"
                  id="floatingUsername"
                  placeholder="sadasfas"
                  value=""
                  name="username"
                />
              </div>
              <div class="form-floating mb-3">
                <label for="floatingPassword">Password</label>
                <input
                  type="password"
                  class="form-control border-black"
                  id="floatingPassword"
                  placeholder=""
                  value=""
                  name="password"
                />
              </div>

              <input type="hidden" name="id">
              <button type="submit" class="btn btn-add btn-primary fw-semibold" onclick="handleSubmit()">
                Submit
              </button>

              <div class="fitur-edit d-none">
                <button type="button" class="btn btn-add btn-secondary fw-semibold" onclick="handleBatal()">
                Batal
              </button>                
              <button type="button" class="btn btn-add btn-primary fw-semibold" onclick="handleUpdate()">
                Simpan
              </button>                
              </div>



              <script>
                const handleSubmit = () => {
                  let name = document.querySelector("input[name=name]").value;
                  let email = document.querySelector("input[name=email]").value;
                  let phone = document.querySelector("input[name=phone]").value;
                  let username = document.querySelector("input[name=username]").value;
                  let password = document.querySelector("input[name=password]").value;

                  if (name && email && phone && username && password) {
                    let user = {name, email, phone, username, password};

                    document.querySelector("input[name=name]").value = "";
                    document.querySelector("input[name=email]").value = "";
                    document.querySelector("input[name=phone]").value = "";
                    document.querySelector("input[name=username]").value = "";
                    document.querySelector("input[name=password]").value = "";

                    fetch(`{{ route("user.store") }}`, {
                      method: "POST", // *GET, POST, PUT, DELETE, etc.
                      mode: "cors", // no-cors, *cors, same-origin
                      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                      credentials: "same-origin", // include, *same-origin, omit
                      headers: {
                        "Content-Type": "application/json",
                        // 'Content-Type': 'application/x-www-form-urlencoded',
                      },
                      redirect: "follow", // manual, *follow, error
                      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                      body: JSON.stringify(user), // body data type must match "Content-Type" header
                    });

                    alert("User Berhasil Ditambah!");
                    getdata();

                  } else {
                    alert("Lengkapi Form!");
                  }


                }

                const handleDelete = (id) => {
                 fetch(`http://localhost:8000/api/user/${id}`, {
                      method: "DELETE", // *GET, POST, PUT, DELETE, etc.
                      mode: "cors", // no-cors, *cors, same-origin
                      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                      credentials: "same-origin", // include, *same-origin, omit
                      headers: {
                        "Content-Type": "application/json",
                        // 'Content-Type': 'application/x-www-form-urlencoded',
                      },
                      redirect: "follow", // manual, *follow, error
                      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                      body: JSON.stringify([]), // body data type must match "Content-Type" header
                    })
                 .then(e => e.json())
                 .then(data => {
                  getdata();
                  alert("Data Berhasil Dihapus!");
                 }); 
                }

                const handleEdit = (id) => {
                 fetch(`http://localhost:8000/api/user/${id}`)
                 .then(e => e.json())
                 .then(data => {

                    document.querySelector(".judul-form").innerHTML = "Edit User";
                    document.querySelector(".btn-add").classList.add("d-none");
                    document.querySelector(".fitur-edit").classList.remove("d-none");
                    document.querySelector("input[name=id]").value = id;
                    document.querySelector("input[name=name]").value = data.name;
                    document.querySelector("input[name=email]").value = data.email;
                    document.querySelector("input[name=phone]").value = data.phone;
                    document.querySelector("input[name=username]").value = data.username;
                    document.querySelector("input[name=password]").value = data.password;

                 }); 
                }

                 const handleBatal = () => {
                    document.querySelector(".judul-form").innerHTML = "Tambah User";
                    document.querySelector(".btn-add").classList.remove("d-none");
                    document.querySelector(".fitur-edit").classList.add("d-none");
                    document.querySelector("input[name=name]").value = "";
                    document.querySelector("input[name=email]").value = "";
                    document.querySelector("input[name=phone]").value = "";
                    document.querySelector("input[name=username]").value = "";
                    document.querySelector("input[name=password]").value = "";
                 }

                  const handleUpdate = () => {
                  let name = document.querySelector("input[name=name]").value;
                  let email = document.querySelector("input[name=email]").value;
                  let phone = document.querySelector("input[name=phone]").value;
                  let username = document.querySelector("input[name=username]").value;
                  let password = document.querySelector("input[name=password]").value;
                  let id = document.querySelector("input[name=id]").value;

                  if (name && email && phone && username && password) {
                    let user = {name, email, phone, username, password};

                    document.querySelector("input[name=name]").value = "";
                    document.querySelector("input[name=email]").value = "";
                    document.querySelector("input[name=phone]").value = "";
                    document.querySelector("input[name=username]").value = "";
                    document.querySelector("input[name=password]").value = "";

                    fetch(`http://localhost:8000/api/user/${id}`, {
                      method: "PUT", // *GET, POST, PUT, DELETE, etc.
                      mode: "cors", // no-cors, *cors, same-origin
                      cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                      credentials: "same-origin", // include, *same-origin, omit
                      headers: {
                        "Content-Type": "application/json",
                        // 'Content-Type': 'application/x-www-form-urlencoded',
                      },
                      redirect: "follow", // manual, *follow, error
                      referrerPolicy: "no-referrer", // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
                      body: JSON.stringify(user), // body data type must match "Content-Type" header
                    }).then(e => {
                      getdata();
                      alert("User Berhasil Diupdate!");
                      document.querySelector(".judul-form").innerHTML = "Tambah User";
                    });


                  } else {
                    alert("Lengkapi Form!");
                  }


                }
              </script>

        </div>
      </div>
    </div>
    
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">User</h4>

          @if (session()->has("success"))
              <div class="alert alert-success">
                {{ session("success") }}
              </div>
          @endif
          
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    #
                  </th>
                  <th>
                    Name
                  </th>
                  <th>
                    Username
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Phone
                  </th>
                  <th>
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody id="tbody">

              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>

<script>
  const tbody = document.querySelector('#tbody');

  const getdata = () => {
    fetch(`{{ route("user.index") }}`).
    then(e => e.json())
    .then(data => {

      let html = ``;
      let no = 1;

      data.forEach(user => {
        html += `
        <tr>
          <td>${no++}</td>
          <td>${user.name}</td>
          <td>${user.username}</td>
          <td>${user.email}</td>
          <td>${user.phone}</td>
          <td>
            <button class="btn btn-sm btn-info" onclick="handleEdit(${user.id})">Edit</button>
            <button class="btn btn-sm btn-danger" onclick="handleDelete(${user.id})">Hapus</button>
          </td>
        </tr>
        `;
      });

      tbody.innerHTML = html;

    })
  }

  getdata();
</script>


@endsection