@extends('dashboard.layout')

@section('content')

<div class="content-wrapper">
  <div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Artikel</h4>

          <a href="{{ route("dashboard.artikel.create") }}" class="btn btn-primary btn-sm d-inline-block mb-3">Tambah Artikel</a>

          @if (session()->has("success"))
              <div class="alert alert-success">
                {{ session("success") }}
              </div>
          @endif
          
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    #
                  </th>
                  <th>
                    Gambar
                  </th>
                  <th>
                    Judul
                  </th>
                  <th>
                    Deskripsi
                  </th>
                  <th>
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody>

                @if ($data)
                    @foreach ($data as $item)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>
                            <img src="{{ asset("assets/img/artikel/" . $item->img) }}" style="width: 100px; border-radius: 0px; height: inherit" alt="">
                          </td>
                          <td>
                            {{ $item->judul }}
                          </td>
                          <td>
                            {{ viewtext( $item->desk, 100 ) }}
                          </td>
                          <td>
                            <a href="{{ route("dashboard.artikel.edit", $item->id) }}" class="btn btn-sm btn-light">Edit</a>

                            <form action="{{ route("dashboard.artikel.destroy", $item->id) }}" class="d-inline" method="POST">
                              @csrf
                              @method("delete")
                              <button class="btn btn-sm btn-danger">Hapus</button>
                            </form>
                          </td>
                        </tr>
                    @endforeach
                @endif
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>

@endsection