@extends('dashboard.layout')

@section('content')

<div class="content-wrapper">
  <div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Edit Artikel</h4>

          <form class="forms-sample" method="POST" action="{{ route("dashboard.artikel.update", $data->id) }}" enctype="multipart/form-data">
            @csrf
            @method("put")

           <div class="form-group">
              <label for="judul">Judul</label>
              <input type="text" class="form-control @error("judul") is-invalid @enderror" id="judul" placeholder="Judul" name="judul" value="{{ old("judul", $data->judul) }}">
            </div>

            <div class="form-group">
              <label for="desk">Deskripsi</label>
              <textarea name="desk" id="desk" cols="30" rows="10" class="form-control @error("desk") is-invalid @enderror">{{ old("desk", $data->desk) }}</textarea>
            </div>

            <div class="form-group">
              <label for="image">Image Baru</label>
              <input type="file" class="form-control" id="image" placeholder="image" name="image">
            </div>


            <button type="submit" class="btn btn-sm btn-primary mr-2">Submit</button>
            <a href="{{ route("dashboard.artikel.index") }}" class="btn btn-sm btn-light">Kembali</a>
          </form>

        </div>
      </div>
    </div>
   
  </div>
</div>

@endsection

