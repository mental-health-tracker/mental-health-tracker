@extends('dashboard.layout')

@section('content')

<div class="content-wrapper">
  <div class="row">
    
    <div class="col-lg-12 grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <h4 class="card-title">Mood</h4>

          @if (session()->has("success"))
              <div class="alert alert-success">
                {{ session("success") }}
              </div>
          @endif
          
          <div class="table-responsive">
            <table class="table table-striped">
              <thead>
                <tr>
                  <th>
                    #
                  </th>
                  <th>
                    User
                  </th>
                  <th>
                    Mood
                  </th>
                  <th>
                    Date
                  </th>
                  <th>
                    Aksi
                  </th>
                </tr>
              </thead>
              <tbody>

                @if ($data)
                    @foreach ($data as $item)
                        <tr>
                          <td>{{ $loop->iteration }}</td>
                          <td>
                            {{ $item->user->name }}
                          </td>
                          <td>
                            {{ $item->mood }}
                          </td>
                          <td>
                            {{ $item->created_at->format('l, d F Y') }} <br />
                            {{ $item->created_at->format('H:i') }} WIB
                          </td>
                          <td>
                            <form action="{{ route("dashboard.mood.destroy", $item->id) }}" class="d-inline" method="POST">
                              @csrf
                              @method("delete")
                              <button class="btn btn-sm btn-danger">Hapus</button>
                            </form>
                          </td>
                        </tr>
                    @endforeach
                @endif
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
   
  </div>
</div>

@endsection