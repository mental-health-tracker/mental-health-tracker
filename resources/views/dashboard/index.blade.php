@extends('dashboard.layout')

@section('content')

<div class="content-wrapper">
  
  <div class="row">
    
    <div class="col-12 d-flex grid-margin stretch-card">
      <div class="card">
        <div class="card-body">
          <div class="d-flex flex-wrap justify-content-between">
            <h4 class="card-title mb-3">Data</h4>
          </div>
          <div class="row">
            <div class="col-12">
              <div class="d-md-flex mb-4">
                <div class="mr-md-5 mb-4">
                  <h5 class="mb-1"><i class="typcn typcn-globe-outline mr-1"></i>Artikel</h5>
                  <h2 class="text-primary mb-1 font-weight-bold">{{ $artikel }}</h2>
                </div>
                <div class="mr-md-5 mb-4">
                  <h5 class="mb-1"><i class="typcn typcn-archive mr-1"></i>Mood</h5>
                  <h2 class="text-secondary mb-1 font-weight-bold">{{ 0 }}</h2>
                </div>
                <div class="mr-md-5 mb-4">
                  <h5 class="mb-1"><i class="typcn typcn-tags mr-1"></i>User</h5>
                  <h2 class="text-warning mb-1 font-weight-bold">{{ 0 }}</h2>
                </div>
              </div>
              <canvas id="salesanalyticChart"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  
  </div>
 
</div>

@endsection