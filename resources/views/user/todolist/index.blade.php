@extends("user.layout")

@section("content")
   
   <main class="pt-5 px-2 mb-5">
      <div class="container text-center">
        <div class="row justify-content-center">
          <div class="col border border-black rounded me-4 p-4 shadow">
            <h2 class="fw-bold">Todolist Anda</h2>
            <div class="container text-center mt-4">

                @if (session()->has("success"))
              <div class="alert alert-success">
                {{ session("success") }}
              </div>
          @endif

              <form action="{{ route("todolist.store") }}" method="POST">
                @csrf

                <div class="d-flex mb-4">
                <input type="text" name="todo" class="form-control">
                <button type="submit" class="btn btn-sm btn-primary">Simpan</button>
                </div>
                
              </form>

              <div class="row">
                <div class="col-8 fw-bold">Todo</div>
                <div class="col fw-bold">Aksi</div>
              </div>

              @foreach($data as $item)
              <div
                class="row mt-2 border border-black rounded py-2 align-items-center"
              >
                <div class="col-8">
                  {{ $item->todo }}
                </div>
                <div class="col">
                  <form action="{{ route("todolist.destroy", $item->id) }}" class="d-inline" method="POST">
                    @csrf
                    @method("delete")
                    <button class="btn btn-sm btn-danger">Hapus</button>
                  </form>
                </div>
              </div>
              @endforeach

            </div>
          </div>
        </div>
      </div>
    </main>

@endsection