@extends("user.layout")

@section("content")
    
    <main class="pt-5 px-5">
      <section id="articles">
        <div class="container-fluid">
          <h1 class="text-center pb-3 fw-bold">{{ $artikel->judul }}</h1>
          <div class="card mb-3 shadow-lg rounded">
            <img
              src="{{ asset('assets/img/artikel/' . $artikel->img) }}"
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              <div class="row gap-5 px-2">
                <div class="col text-start">
                  <h3 class="card-title fw-bold">{{ $artikel->judul }}</h3>
                  <p class="card-text fs-4">
                    {{ $artikel->desk }}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>

        
      </section>
    </main>

@endsection