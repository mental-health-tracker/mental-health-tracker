@extends("user.layout")



@section("content")

@php
  function v($str, $len) {
    $text = "";
    for($i = 0; $i < $len; $i++) {
      if(isset($str[$i])) {
        $text .= $str[$i];
      } else {
        break;
      }

    }

    $text .= "...";
    return $text;
  }
@endphp
    
    <main class="pt-5 px-5">
      <section id="articles">
        <div class="container-fluid">
          <h1 class="text-center pb-3 fw-bold">{{ $artikel[0]->judul }}</h1>
          <div class="card mb-3 shadow-lg rounded">
            <img
              src="{{ asset("assets/img/artikel/" . $artikel[0]->img) }}"
              class="card-img-top"
              alt="..."
            />
            <div class="card-body">
              <div class="row gap-5 px-2">
                <div class="col text-start">
                  <h1 class="card-title fw-bold">{{ $artikel[0]->judul }}</h1>
                  <p class="card-text fs-4">
                    {{ $artikel[0]->desk }}
                  </p>
                </div>
                <div class="col">
                  <img src="assets/img/Rectangle 10.png" alt="" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="container-fluid text-center mt-5 pb-5">
          <h1>RECOMMENDATION ARTICLES</h1>
          <div id="carouselExample" class="carousel slide mt-4">
            <div class="carousel-inner">

              @php

              @endphp

              @php
                $html = "<div class='carousel-item active'>
              <div class='d-flex justify-content-center flex-wrap gap-3'>";
                $no = 1;
              @endphp

              @foreach($artikel as $item)


                @if($loop->iteration > 1)
                  @php
                    $html .= "<div class='card d-block col-6 col-md-4 col-lg-3'>
                    <img
                      src='" . asset( "assets/img/artikel/" . $item->img ) . "'
                      class='card-img-top'
                      alt='...'
                    />
                    <div class='card-body'>
                      <h5 class='card-title'>" . $item->judul . "</h5>
                      <p class='card-text'>
                        " .  v( $item->desk, 100 ) . "
                      </p>
                      <a href='" . route( 'artikel-detail' , $item->id ) . "' class='btn btn-sm btn-primary'>Detail</a>
                    </div>
                  </div>";
                  $no++;
                  @endphp
                @endif

                @if($no > 3)
                  @php
                    $html .= "</div>
                  </div>";
                    echo $html;
                    $no = 1;
                    $html = '';
                    $html = "<div class='carousel-item'>
                    <div class='d-flex justify-content-center flex-wrap gap-3'>";
                  @endphp
                @endif

              @endforeach

              @php
                $html .= "</div>
              </div>";
              @endphp

              @php
                echo $html;
              @endphp



             {{--  <div class="carousel-item active">
                <div class="d-flex justify-content-center flex-wrap gap-3">
                  <div class="card d-block col-6 col-md-4 col-lg-3">
                    <img
                      src="assets/img/Rectangle 8.png"
                      class="card-img-top"
                      alt="..."
                    />
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                  </div>
                  <div class="card d-block col-6 col-md-4 col-lg-3">
                    <img
                      src="assets/img/Rectangle 8.png"
                      class="card-img-top"
                      alt="..."
                    />
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                  </div>
                  <div class="card d-block col-6 col-md-4 col-lg-3">
                    <img
                      src="assets/img/Rectangle 8.png"
                      class="card-img-top"
                      alt="..."
                    />
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                  </div>
                </div>
              </div>
              <div class="carousel-item">
                <div class="d-flex justify-content-center flex-wrap gap-3">
                  <div class="card d-block col-6 col-md-4 col-lg-3">
                    <img
                      src="assets/img/Rectangle 8.png"
                      class="card-img-top"
                      alt="..."
                    />
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                  </div>
                  <div class="card d-block col-6 col-md-4 col-lg-3">
                    <img
                      src="assets/img/Rectangle 8.png"
                      class="card-img-top"
                      alt="..."
                    />
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                  </div>
                  <div class="card d-block col-6 col-md-4 col-lg-3">
                    <img
                      src="assets/img/Rectangle 8.png"
                      class="card-img-top"
                      alt="..."
                    />
                    <div class="card-body">
                      <h5 class="card-title">Card title</h5>
                      <p class="card-text">
                        Some quick example text to build on the card title and
                        make up the bulk of the card's content.
                      </p>
                      <a href="#" class="btn btn-primary">Go somewhere</a>
                    </div>
                  </div>
                </div>
              </div>
            </div> --}}
            <button
              class="carousel-control-prev"
              type="button"
              data-bs-target="#carouselExample"
              data-bs-slide="prev"
            >
              <span
                class="carousel-control-prev-icon"
                aria-hidden="true"
                style="filter: invert(100%)"
              ></span>
              <span class="visually-hidden">Previous</span>
            </button>
            <button
              class="carousel-control-next"
              type="button"
              data-bs-target="#carouselExample"
              data-bs-slide="next"
            >
              <span
                class="carousel-control-next-icon"
                aria-hidden="true"
                style="filter: invert(100%)"
              ></span>
              <span class="visually-hidden">Next</span>
            </button>
          </div>
        </div>
      </section>
    </main>

@endsection


