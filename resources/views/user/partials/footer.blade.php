<footer class="text-light">
      <div class="py-5" style="background-color: #05254e">
        <div class="container">
          <div class="row">
            <div class="col-md-4 col-sm-12">
              <h3>About Us</h3>
              <p>
                Welcome to Mental Health Tracker, an innovative solution to help
                you care for your mental well-being better. We understand how
                important it is to maintain mental health, and that's why we are
                here to support your journey towards better mental well-being.
                With the range of features we offer, you can easily manage and
                monitor your mental health.
              </p>
            </div>
            <div class="col-md-4 col-sm-12 mt-4 mt-md-0">
              <h3>Follow Us</h3>
              <div class="social-media-icons">
                <a href="#" style="color: white; font-size: 2vw"
                  ><i class="fab fa-facebook"></i
                ></a>
                <a href="#" style="color: white; font-size: 2vw"
                  ><i class="fab fa-instagram"></i
                ></a>
                <a href="#" style="color: white; font-size: 2vw"
                  ><i class="fab fa-twitter"></i
                ></a>
                <a href="#" style="color: white; font-size: 2vw"
                  ><i class="fab fa-youtube"></i
                ></a>
              </div>
            </div>
            <div class="col-md-4 col-sm-12 mt-4 mt-md-0">
              <h3>Contact Us</h3>
              <p>Email: info@mentalhealthtracker.com</p>
              <p>Phone: +123 456 7890</p>
            </div>
          </div>
        </div>
      </div>
      <div class="py-2" style="background-color: #134172">
        <div class="container">
          <div class="row">
            <div class="col-md-6 col-sm-12">
              <p>&copy; 2023 Mental Health Tracker. All rights reserved.</p>
            </div>
            <div class="col-md-6 col-sm-12 text-end">
              <p>
                Pemrograman Web • MM1 • Kelompok 9 • Handcrafted with love by
                Dian Maharani | Muhammad Anggoro | Bagas Yuli | Dimas | Nanda
              </p>
            </div>
          </div>
        </div>
      </div>
    </footer>
    <script src="{{ asset('assets/js/global.js') }}"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"></script>