<nav
        class="navbar navbar-expand-lg navbar-dark bg-transparent fixed-top px-5"
      >
        <div class="container-fluid">
          <a class="navbar-brand" href="#"
            ><img src="{{ asset('assets/img/logo.png') }}" alt="Logo" style="max-height: 70px"
          /></a>
          <button
            class="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarNav"
            aria-controls="navbarNav"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span class="navbar-toggler-icon"></span>
          </button>
          <div class="collapse navbar-collapse" id="navbarNav">
            <ul class="navbar-nav ms-auto fs-5">
              <li class="nav-item">
                <a
                  class="nav-link active text-light"
                  aria-current="page"
                  href="{{ route('beranda') }}"
                  >Home</a
                >
              </li>
              <li class="nav-item">
                <a class="nav-link text-light" href="{{ route('mood') }}">Track Mood</a>
              </li>
              <li class="nav-item me-2">
                <a class="nav-link text-light" href="{{ route('artikel') }}">Articles</a>
              </li>
              <li class="nav-item me-2">
                <a class="nav-link text-light" href="{{ route('todolist.index') }}">Todolist</a>
              </li>
              <li class="nav-item me-2">
                <a class="nav-link text-light" href="{{ route('logout') }}">Logout</a>
              </li>
              <li class="nav-item">
                <a  type="button"
                  class="btn btn-m btn-primary mt-1"
                  style="font-weight: 600" href="{{ route('signin') }}">
                  Log In
                </a>
              </li>
            </ul>
          </div>
        </div>
      </nav>