@extends("user.layout")

@php
  function viewtext($str, $len) {
    $text = "";
    for($i = 0; $i < $len; $i++) {
      if(isset($str[$i])) {
        $text .= $str[$i];
      } else {
        break;
      }

    }

    $text .= "...";
    return $text;
  }
@endphp

@section("content")
   
   <main class="pt-5 px-2 mb-5">
      <div class="container text-center">
        <div class="row justify-content-center">
          <div class="col border border-black rounded me-4 p-4 shadow">
            <h2 class="fw-bold">Record Track Mood</h2>
            <div class="container text-center mt-4">

               @if (session()->has("success"))
                  <div class="alert alert-success">
                    {{ session("success") }}
                  </div>
              @endif

              <div class="row">
                <div class="col-8 fw-bold">Date</div>
                <div class="col fw-bold">Mood</div>
              </div>

              @foreach($mood as $item)
              <div
                class="row mt-2 border border-black rounded py-2 align-items-center"
              >
                <div class="col-8">
                  {{ $item->created_at->format('l, d F Y') }} <br />
                  {{ $item->created_at->format('H:i') }} WIB
                </div>
                <div class="col">{{ $item->mood }}</div>
              </div>
              @endforeach

            </div>
          </div>
          <div class="col-8 border border-black rounded p-4 shadow">
            <h2 class="fw-bold">Account Information</h2>
            <div
              class="row gap-5 mx-4 mt-4 p-5 rounded align-items-center mb-5"
              style="background-color: #1f5790"
            >
              <div class="col">
                <img
                  src="assets/img/dian.png"
                  style="border-radius: 50%"
                  alt=""
                />
              </div>
              <div class="col text-start">
                <h2 class="fw-bold text-light">{{ $user->name }}</h2>
                <p class="fs-4 fw-bold text-light">ID {{ $user->id }}</p>
              </div>
            </div>
            <div class="container px-4 text-center mt-5">

              <form action="{{ route("update_profile") }}" method="POST">
                @csrf
              <div class="row gx-5">
                <div class="col">
                  <div class="form-floating mb-3">
                    <input
                      type="text"
                      class="form-control border-black"
                      id="floatingName"
                      placeholder="asdsad"
                      value="{{ $user->name }}"
                      name="name"
                    />
                    <label for="floatingName">Name</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input
                      type="number"
                      class="form-control border-black"
                      id="floatingPhone"
                      placeholder="0341341"
                      value="{{ $user->phone }}"
                      name="phone"
                    />
                    <label for="floatingPhone">Phone number</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input
                      type="text"
                      class="form-control border-black"
                      id="floatingID"
                      placeholder="2J1NF1"
                      readonly
                      value="{{ $user->id }}"
                    />
                    <label for="floatingID">ID</label>
                  </div>
                </div>
                <div class="col">
                  <div class="form-floating mb-3">
                    <input
                      type="text"
                      class="form-control border-black"
                      id="floatingLocation"
                      placeholder="purwokerto"
                      value="{{ $user->location }}"
                      name="location"
                    />
                    <label for="floatingLocation">Location</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input
                      type="text"
                      class="form-control border-black"
                      id="floatingUsername"
                      placeholder="sadasfas"
                      value="{{ $user->username }}"
                      name="username"
                    />
                    <label for="floatingUsername">Username</label>
                  </div>
                  <div class="form-floating mb-3">
                    <input
                      type="email"
                      class="form-control border-black"
                      id="floatingInput"
                      placeholder="name@example.com"
                      value="{{ $user->email }}"
                      name="email"
                    />
                    <label for="floatingInput">Email address</label>
                  </div>
                </div>
              </div>
              <button type="submit" class="btn btn-primary btn-lg float-end fw-semibold">
                Save
              </button>
              </form>

            </div>
          </div>
        </div>
      </div>
    </main>

@endsection