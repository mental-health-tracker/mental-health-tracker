<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>MHT</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
    />
    <link rel="stylesheet" href="{{ asset('assets/css/global.css') }}" />
  </head>
  <body>
    <header
      style="
        background: url('{{ asset('assets/img/head.png') }}') no-repeat center center;
        background-size: cover;
        padding-top: 15%;
        padding-bottom: 10%;
        min-height: 600px;
      "
    >
      @include("user.partials.navbar")
    </header>

    @yield('content')

    @include("user.partials.footer")
  </body>
</html>
