<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form</title>
    <link
      href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css"
      rel="stylesheet"
    />
    <link
      rel="stylesheet"
      href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css"
    />
    <link rel="stylesheet" href="/asset/css/global.css" />
  </head>
  <body>
    <main class="container-fluid d-flex align-items-center mt-5">
      <div id="signup" class="card mx-auto mt" style="width: 65%; display: none">
        <div class="row g-0 align-items-center">
          <div class="col-md-6">
            <img
              src="/assets/img/bgSignup.png"
              class="img-fluid rounded-start"
              style="background-size: cover"
              alt="..."
            />
          </div>
          <div class="col-md-6">
            <div class="card-body d-grid">
              <h1 class="card-title text-center fw-bold mb-5">Sign Up</h1>
              <h4>Register Now!</h4>


              <form action="{{ route("register") }}" method="POST">
                @csrf
              <div class="form-floating mb-3">
                <input
                  class="form-control border-black"
                  type="text"
                  id="fullname"
                  placeholder="Masukkan nama"
                  value=""
                  name="name"
                />
                <label for="fullname">Full Name</label>
              </div>
              <div class="form-floating mb-3">
                <input
                  type="email"
                  class="form-control border-black"
                  id="floatingInput"
                  placeholder="name@example.com"
                  value=""
                  name="email"
                />
                <label for="floatingInput">Email address</label>
              </div>
              <div class="form-floating mb-3">
                <input
                  type="number"
                  class="form-control border-black"
                  id="floatingPhone"
                  placeholder="0341341"
                  value=""
                  name="phone"
                />
                <label for="floatingPhone">Phone number</label>
              </div>
              <div class="form-floating mb-3">
                <input
                  type="text"
                  class="form-control border-black"
                  id="floatingUsername"
                  placeholder="sadasfas"
                  value=""
                  name="username"
                />
                <label for="floatingUsername">Username</label>
              </div>
              <div class="form-floating mb-3">
                <input
                  type="password"
                  class="form-control border-black"
                  id="floatingPassword"
                  placeholder=""
                  value=""
                  name="password"
                />
                <label for="floatingPassword">Password</label>
              </div>
              <button type="submit" class="btn btn-primary fw-semibold">
                Submit
              </button>
              </form>
              <p>
                Already have an account? <a id="loginow" href="">Log in now</a>
              </p>
            </div>
          </div>
        </div>
      </div>
      <div id="login" class="card mx-auto mt" style="width: 65%; display: block">
        <div class="row g-0 align-items-center">
          <div class="col-md-6">
            <div class="card-body d-grid">
              <h1 class="card-title text-center fw-bold mb-5">Log In</h1>
              <h4>Please Login to Your Account</h4>

              @if (session()->has("error"))
                  <div class="alert alert-danger">
                    {{ session("error") }}
                  </div>
              @endif

              <form action="{{ route("auth") }}" method="POST">
                @csrf
              <div class="form-floating mb-3">
                <input
                  type="text"
                  class="form-control border-black"
                  id="floatingUsername"
                  placeholder="sadasfas"
                  value=""
                  name="username"
                />
                <label for="floatingUsername">Username</label>
              </div>
              <div class="form-floating mb-3">
                <input
                  type="password"
                  class="form-control border-black"
                  id="floatingPassword"
                  placeholder=""
                  value=""
                  name="password"
                />
                <label for="floatingPassword">Password</label>
              </div>
              <button type="submit" class="btn btn-primary fw-semibold">
                Submit
              </button>

              </form>

              <p>
                Don't have an account? <a id="signupnow" href="">Sign up now</a>
              </p>
            </div>
          </div>
          <div class="col-md-6">
            <img
              src="/assets/img/bgLogin.png"
              class="img-fluid rounded-start"
              style="background-size: cover"
              alt="..."
            />
          </div>
        </div>
      </div>
    </main>
    <script>
      document
        .getElementById("loginow")
        .addEventListener("click", function (e) {
          e.preventDefault();
          document.getElementById("signup").style.display = "none";
          document.getElementById("login").style.display = "block";
        });

      document
        .getElementById("signupnow")
        .addEventListener("click", function (e) {
          e.preventDefault();
          document.getElementById("login").style.display = "none";
          document.getElementById("signup").style.display = "block";
        });
    </script>
  </body>
</html>
