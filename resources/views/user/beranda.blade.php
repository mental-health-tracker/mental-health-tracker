@extends("user.layout")



@section("content")

    <main class="px-5">
      <div class="container-fluid text-center mt-5 pb-5 px-5">
        <div class="row gap-5 px-5">
          <div class="col">
            <img src="{{ asset('assets/img/Rectangle 10.png') }}" alt="" />
          </div>
          <div class="col text-start">
            <h2 class="mb-4">MENTAL HEALTH TRACKER</h2>
            <p style="font-size: 17px; text-align: justify">
              Mental Health Tracker adalah website yang dapat membatu kamu untuk
              mengetahui mood yang ada rasakan dalam satu hari dan akan kami
              berikan chart untuk dapat mengetahui riwayat dan presentase mood
              kamu dalam satu bulan. Hal ini kami lakukan sebagai upaya
              pencegahan gangguan kesehatan mental yang ada di lingkungan
              kampus.
            </p>
            <p style="font-size: 17px; text-align: justify">
              Mental Health Tracker adalah website yang dapat membatu kamu untuk
              mengetahui mood yang ada rasakan dalam satu hari dan akan kami
              berikan chart untuk dapat mengetahui riwayat dan presentase mood
              kamu dalam satu bulan. Hal ini kami lakukan sebagai upaya
              pencegahan gangguan kesehatan mental yang ada di lingkungan
              kampus.
            </p>
            <button type="button" class="btn btn-dark">Gabung Komunitas</button>
          </div>
        </div>
      </div>

      <div class="container-fluid text-center mt-5 pb-5">
        <h1>CHOOSE YOUR MOOD TODAY</h1>
        <div class="container text-center mt-4">
          <div class="row justify-content-center gap-5">
            <div
              class="col-lg-2 col-md-4 col-sm-6 p-4 rounded-4 shadow-lg"
              style="background-color: #c0d6ec"
            >
              <div class="row">
                <div class="col mb-4">
                  <i
                    class="fa-solid fa-face-smile img-fluid"
                    style="color: whitesmoke; font-size: 8vw"
                  ></i>
                </div>
              </div>
              <div class="row">
                <div for="happy" class="col d-grid">
                  <a href="{{ route("mood-add", "Happy") }}" class="btn btn-light"
                    style="font-weight: 600">
                    HAPPY
                  </a>
                </div>
              </div>
            </div>
            <div
              class="col-lg-2 col-md-4 col-sm-6 p-4 rounded-4 shadow-lg"
              style="background-color: #c0d6ec"
            >
              <div class="row">
                <div class="col mb-4">
                  <i
                    class="fa-solid fa-face-meh img-fluid"
                    style="color: whitesmoke; font-size: 8vw"
                  ></i>
                </div>
              </div>
              <div class="row">
                <div for="neutral" class="col d-grid">
                  <a href="{{ route("mood-add", "Netral") }}" class="btn btn-light"
                    style="font-weight: 600">
                    NETRAL
                  </a>
                </div>
              </div>
            </div>
            <div
              class="col-lg-2 col-md-4 col-sm-6 p-4 rounded-4 shadow-lg"
              style="background-color: #c0d6ec"
            >
              <div class="row">
                <div class="col mb-4">
                  <i
                    class="fa-solid fa-face-sad-tear img-fluid"
                    style="color: whitesmoke; font-size: 8vw"
                  ></i>
                </div>
              </div>
              <div class="row">
                <div for="sad" class="col d-grid">
                  <a href="{{ route("mood-add", "Sad") }}" class="btn btn-light"
                    style="font-weight: 600">
                    SAD
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container-fluid text-center mt-5 pb-5">
        <h1>YOUR MOOD TODAY FOR A MONTH</h1>
        <div class="container text-center mt-4">
          <div class="row justify-content-center gap-5">
            <div
              class="col-lg-3 col-md-4 col-sm-6 p-4 me-4 shadow"
              style="background-color: #74a4d3"
            >
              <div class="row">
                <div class="col mb-4">
                  <i
                    class="fa-solid fa-face-smile"
                    style="color: whitesmoke; font-size: 6vw"
                  ></i>
                </div>
              </div>
              <div class="row">
                <div class="col d-grid">
                  <h2>HAPPY</h2>
                  <p>Mood Kamu Bulan December ini Happy Sebanyak {{ $happy }}%</p>
                  <div
                    class="progress"
                    role="progressbar"
                    aria-valuenow="{{ $happy }}"
                    aria-valuemin="0"
                    aria-valuemax="100"
                  >
                    <div
                      class="progress-bar bg-info text-dark progress-bar-striped progress-bar-animated"
                      style="width: {{ $happy }}%"
                      id="happyProgress"
                    >
                      <span id="happyPercentage">{{ $happy }}%</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="col-lg-3 col-md-4 col-sm-6 p-4 shadow"
              style="background-color: #74a4d3"
            >
              <div class="row">
                <div class="col mb-4">
                  <i
                    class="fa-solid fa-face-meh"
                    style="color: whitesmoke; font-size: 6vw"
                  ></i>
                </div>
              </div>
              <div class="row">
                <div class="col d-grid">
                  <h2>NETRAL</h2>
                  <p>Mood Kamu Bulan December ini Netral Sebanyak {{ $netral }}%</p>
                  <div
                    class="progress"
                    role="progressbar"
                    aria-valuenow="{{ $netral }}"
                    aria-valuemin="0"
                    aria-valuemax="100"
                  >
                    <div
                      class="progress-bar bg-info text-dark progress-bar-striped progress-bar-animated"
                      style="width: {{ $netral }}%"
                      id="neutralProgress"
                    >
                      <span id="neutralPercentage">{{ $netral }}%</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div
              class="col-lg-3 col-md-4 col-sm-6 p-4 ms-4 shadow"
              style="background-color: #74a4d3"
            >
              <div class="row">
                <div class="col mb-4">
                  <i
                    class="fa-solid fa-face-sad-tear"
                    style="color: whitesmoke; font-size: 6vw"
                  ></i>
                </div>
              </div>
              <div class="row">
                <div class="col d-grid">
                  <h2>SAD</h2>
                  <p>Mood Kamu Bulan December ini Sad Sebanyak {{ $sad }}%</p>
                  <div
                    class="progress"
                    role="progressbar"
                    aria-valuenow="{{ $sad }}"
                    aria-valuemin="0"
                    aria-valuemax="100"
                  >
                    <div
                      class="progress-bar bg-info text-dark progress-bar-striped progress-bar-animated"
                      style="width: {{ $sad }}%"
                      id="sadProgress"
                    >
                      <span id="sadPercentage">{{ $sad }}%</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="container-fluid text-center mt-5 pb-5 px-5">
        <h1>RECOMMENDATION ARTICLES</h1>
        <form class="d-flex mb-5 justify-content-center" role="search">
          <input
            class="form-control me-2 w-50"
            style="background-color: #c0d6ec"
            type="search"
            placeholder="Search"
            aria-label="Search"
          />
          <button class="btn btn-outline-primary" type="submit">Search</button>
        </form>
        <div
          href=""
          style="cursor: pointer"
          class="row row-cols-1 row-cols-md-3 g-4 px-5"
        >

        @foreach($artikel as $item)

          <div class="col">
            <a href="{{ route('artikel-detail', $item->id) }}" style="text-decoration: none;">
            <div class="card h-100">
              <img
                src="{{ asset('assets/img/artikel/' . $item->img) }}"
                class="card-img-top"
                alt="..."
              />
              <div class="card-body">
                <h5 class="card-title text-dark" >{{ $item->judul }}</h5>
                <p class="text-dark">{{ viewtext( $item->desk, 100 ) }}</p>
              </div>
            </a>
            </div>
          </div>

          @endforeach

      </div>
    </main>

@endsection