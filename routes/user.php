<?php

use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\BookController;
use App\Http\Controllers\DashboardAdminController;
use App\Http\Controllers\DashboardUserController;
use App\Http\Controllers\KonsultasiController;
use App\Http\Controllers\CeritaController;
use App\Http\Controllers\CurhatController;
use App\Http\Controllers\MotivasiController;
use App\Http\Controllers\MusicController;
use App\Http\Controllers\TourController;
use Illuminate\Support\Facades\Route;

Route::get('/', [BerandaController::class, 'index'])->name("beranda");

Route::get('/signin', [AuthController::class, 'index'])->name("signin");
Route::post('/signin', [AuthController::class, 'auth'])->name("auth");

Route::get('/signup', [AuthController::class, 'signup'])->name("signup");
Route::post('/signup', [AuthController::class, 'register'])->name("register");

Route::prefix('user')->name('user.')->middleware("auth")->group(function () {

  Route::get('/', [DashboardUserController::class, 'index'])->name("index");
  Route::get('/rekomendasi', [DashboardUserController::class, 'rekomendasi'])->name("rekomendasi");
  Route::get('/about', [DashboardUserController::class, 'about'])->name("about");
  Route::get('/informasi', [DashboardUserController::class, 'informasi'])->name("informasi");

  Route::get('/konsultasi', [KonsultasiController::class, 'index'])->name("konsultasi");
  Route::get('/konsultasi/create', [KonsultasiController::class, 'create'])->name("konsultasi.create");
  Route::post('/konsultasi/store', [KonsultasiController::class, 'store'])->name("konsultasi.store");

  Route::get('/cerita/create', [CeritaController::class, 'create'])->name("cerita.create");
  Route::post('/cerita', [CeritaController::class, 'store'])->name("cerita.store");

  Route::get('/curhat', [CurhatController::class, 'index'])->name("curhat.index");
  Route::post('/curhat', [CurhatController::class, 'store'])->name("curhat.store");

});
