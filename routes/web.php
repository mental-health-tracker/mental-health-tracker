<?php

use App\Http\Controllers\AdminAuthController;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\BerandaController;

use App\Http\Controllers\MoodController;
use App\Http\Controllers\ArtikelController;
use App\Http\Controllers\TodolistController;
use App\Http\Controllers\UserController;

use App\Http\Controllers\DashboardAdminController;
use App\Http\Controllers\DashboardUserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [BerandaController::class, 'index'])->name("beranda")->middleware("auth");
Route::get('/track-mood', [BerandaController::class, 'mood'])->name("mood")->middleware("auth");
Route::get('/article', [BerandaController::class, 'artikel'])->name("artikel")->middleware("auth");
Route::get('/article/{artikel}', [BerandaController::class, 'artikel_detail'])->name("artikel-detail")->middleware("auth");
Route::post('/profile', [BerandaController::class, 'update_profile'])->name("update_profile");

Route::get('/mood/{type}', [MoodController::class, 'store'])->name("mood-add")->middleware("auth");

Route::resource('/todolist', TodolistController::class)->middleware("auth");


Route::get('/signin', [AuthController::class, 'login'])->name("signin");
Route::post('/sigin', [AuthController::class, 'auth'])->name("auth");
Route::get('/signup', [AuthController::class, 'register'])->name("signup");
Route::post('/signup', [AuthController::class, 'store'])->name("register");
Route::get('/logout', [AuthController::class, 'logout'])->name("logout");

Route::prefix('dashboard')->name('dashboard.')->group(function () {
  
  Route::get('/', function() {
    return redirect("dashboard/artikel");
  })->name("index");

  Route::get('/signin', [AdminAuthController::class, 'index'])->name("signin");
  Route::post('/signin', [AdminAuthController::class, 'auth'])->name("auth");
  Route::get('/logout', [AdminAuthController::class, 'logout'])->name("logout");

  Route::resource('/mood', MoodController::class);
  Route::resource('/artikel', ArtikelController::class);

  Route::get('/user', [UserController::class, 'index'])->name("user.index");

  Route::get('/logout', [AdminAuthController::class, 'logout'])->name("logout");
});

