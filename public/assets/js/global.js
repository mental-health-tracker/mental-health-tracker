// mood
let percenthappy = document.getElementById("happyPercentage");
let percentneutral = document.getElementById("neutralPercentage");
let percentsad = document.getElementById("sadPercentage");
let progreshappy = document.getElementById("happyProgress");
let progresneutral = document.getElementById("neutralProgress");
let progressad = document.getElementById("sadProgress");
let moodData = {
  happy: 0,
  neutral: 0,
  sad: 0,
  totalDays: 0,
};

function updateMood(mood) {
  if (moodData.totalDays < 30) {
    moodData[mood]++;
    moodData.totalDays++;

    updateUI();
  } else {
    alert("30-day tracking completed!");
  }
}

function updateUI() {
  percenthappy.innerText = `${calculatePercentage("happy")}%`;
  percentneutral.innerText = `${calculatePercentage("neutral")}%`;
  percentsad.innerText = `${calculatePercentage("sad")}%`;

  progreshappy.style.width = `${calculatePercentage("happy")}%`;
  progresneutral.style.width = `${calculatePercentage("neutral")}%`;
  progressad.style.width = `${calculatePercentage("sad")}%`;
}

function calculatePercentage(mood) {
  const totalMoods = moodData.happy + moodData.neutral + moodData.sad;
  const moodPercentage = ((moodData[mood] / totalMoods) * 100).toFixed(2) || 0;
  return moodPercentage;
}

// navbar
const navbar = document.querySelector("nav"); // Menggunakan querySelector untuk mendapatkan elemen pertama dengan tagname "nav"
window.addEventListener("scroll", function () {
  console.log(window.scrollY); // Menggunakan window.scrollY, bukan window.scrolly
  if (this.window.scrollY > 1) {
    navbar.classList.replace("bg-transparent", "nav-color");
  } else if (this.window.scrollY <= 0) {
    navbar.classList.replace("nav-color", "bg-transparent");
  }
});
//form
const form = document.querySelector("form");
const usernameInput = document.querySelector("#username");
const passwordInput = document.querySelector("#password");
const loginButton = document.querySelector(".btn-primary");
const errorMessage = document.querySelector(".error-message");
// form.addEventListener("submit", (event) => {
//   event.preventDefault();
//   const username = usernameInput.value.trim();
//   const password = passwordInput.value.trim();
//   if (!username || !password) {
//     errorMessage.textContent = "Please fill in both fields";
//     return;
//   }
//   if (username === "admin" && password === "password") {
//     errorMessage.textContent = "Welcome, you are now logged in!";
//     setTimeout(() => {
//       window.location.href = "dashboard.html";
//     }, 2000);
//   } else {
//     errorMessage.textContent = "Incorrect username or password";
//   }
// });
